#!/bin/bash
cd /root/bloom_repos/bloom-build/
git pull origin master
cd ~
rm -rf /root/bloom_compose/dists/europeennes.bloomassociation.org/*
cp -rfv /root/bloom_repos/bloom-build/* /root/bloom_compose/dists/europeennes.bloomassociation.org/
cd bloom_compose
docker-compose up --build -d

